
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate after type ]*/
    $('.validate-input .input100').each(function () {
        $(this).on('blur', function () {
            if (validate(this) == false) {
                showValidate(this);
            }
            else {
                $(this).parent().addClass('true-validate');
            }
        })
    })


    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit', function () {
        var check = true;
        const post = new FormData();
        for (var i = 0; i < input.length; i++) {
            if (validate(input[i]) == false) {
                showValidate(input[i]);
                check = false;
            }
        }
        // console.log("oi");
        post.append("newPassword", $("input[name='password']").val());
        post.append("reset_id", window.location.search.replace("?reset_id=", ""));
        // fetch("http://localhost/nutring-api/confirmPasswordChange", {
        //     method: 'POST',
        //     headers: {
        //       'Content-Type': 'application/x-www-form-urlencoded'
        //     },
        //     body: post
        // })

        $.post("http://localhost/nutring-api/confirmPasswordChange",
            {
                newPassword: $("input[name='password']").val(),
                reset_id: window.location.search.replace("?reset_id=", "")
            },
            function (data, status) {
                let result = JSON.parse(data)
                if (result.result == "INVALID_TOKEN") {
                    $('.validate-form').html(`
                    <span class="login100-form-title">
                        Esse Token está expirado. 
                        Abra o nutring para pedir um novo link de reset!
                    </span>
                    `)
                } else {
                    $('.validate-form').html(`
                    <span class="login100-form-title">
                        Sua senha foi redefina com sucesso!<br>
                        Agora é só apreveitar o Nutring!
                    </span>
                    `)
                }

            });
        return false;
    });


    $('.validate-form .input100').each(function () {
        $(this).focus(function () {
            hideValidate(this);
            $(this).parent().removeClass('true-validate');
        });
    });

    function validate(input) {
        if ($(input).attr('type') == 'email' || $(input).attr('name') == 'password-confirmation') {
            if ($(input).val() != $("input[name='password']").val()) {
                return false;
            } else {
                $('.login100-form-btn').prop('disabled', false);
            }
        }
        else {
            console.log("else");
            if ($(input).val().trim() == '') {
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');

        $(thisAlert).append('<span class="btn-hide-validate">&#xf135;</span>')
        $('.btn-hide-validate').each(function () {
            $(this).on('click', function () {
                hideValidate(this);
            });
        });
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
        $(thisAlert).find('.btn-hide-validate').remove();
    }



})(jQuery);